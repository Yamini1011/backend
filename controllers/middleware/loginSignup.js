const db_con = require("../../config");

var jwt = require('jsonwebtoken');
const bcrypt = require("bcrypt");






exports.signup = (req, res) => {

  const name=req.body.name;
const email=req.body.email;
const password=req.body.password;


  if(!name || !email || !password ){

    return res.status(400).json({
        status: false,
        message: "Please enter all field",
        status:400,
      
      });
  }else{

    db_con.query(
      "select email from users where email=?", [email],(error, result) => {
        if (error) {
          console.log(error);
        }
        else{
          if(result.length){
            if (result.length > 0) {
                return res.status(400).json({
                    status: false,
                    message: "Enter email already exits",
                    status:400,
                
                  });
        
              } 
    
          }
       
          else{
            bcrypt.hash(password, 10, function (err, hash) {
  
              const token = jwt.sign({ id: email }, "yaminisharma");
      
              var user_data = {name: name,email: email,password: hash,token: token }
              db_con.query(
                "INSERT INTO users SET ?", user_data,
      
                (error, result) => {
      
                  if (error) {
                    console.log(error);
                  } else {
      
                     return res.status(200).json({
                      status: false,
                      message: "Register Successfully",
                      status:200,
                      
                    });
                  }
                }
              );
            });
          }

        } 
      
      }
    );

  }
  
};

exports.login = async (req, res) => {
  try {
    const { email, password } = req.body;
    console.log(password)
    if (!email || !password) {
        return res.status(400).json({
            status: false,
            message: "Please enter all filled",
            status:400,
          
          });
    }else{

      db_con.query(
        "select * from users where email=?",
        [email],
        async (error, result) => {
          if (result.length > 0) {
            if (!(await bcrypt.compare(password, result[0].password))) {
          
              return res.status(400).json({
                  status: false,
                  message: "Incorrect password",
                  status:400,
                
                });
            } else {
              const id = result[0].id;
              const token = jwt.sign({ id: id }, "yaminisharma");
              console.log("the token is " + token);
              const cookie_ = {
                expires: new Date(
                  Date.now() + 2000000
  
                ),
                httpOnly: true
  
              }
              res.cookie('jwt', token, cookie_);
              return res.status(200).json({
                  status: false,
                  message: "Login Successfully",
                  status:200,
                
                });
            }
          } else {
              return res.status(400).json({
                  status: false,
                  message: "User does not exist",
                  status:400,
                
                });
          }
  
        }
      );

    }

  
  } catch (error) {
    console.log(error);
  }

};



exports.logout = async (req, res, next) => {
  try {
    res.clearCookie('jwt');
    return res.status(200).json({
        status: true,
        message: "Logout Successfully",
      
      
      });
  } catch (error) {
    return res.status(500).send(error)
  }

}


