const express = require("express");
const app = express();
const dbcon = require("./config")
const cookieParser = require('cookie-parser');
const port = 2200       ;
app.use(cookieParser())
app.use(express.urlencoded({ extended: false }));
app.use(express.json())

var cors = require('cors');
app.use(cors());
app.use(express.static('uploads'));   
app.use('/uploads', express.static('uploads')); 

app.use('/',require('./route/routes'));
app.use('/',require('./controllers/middleware/product'));

app.listen(port, () => {
    console.log(" Server start on " + port)
})