const express=require('express');
const router=express.Router();
const loginSignup=require('../controllers/middleware/loginSignup')


router.post('/addUser',loginSignup.signup);


router.post('/loginUser',loginSignup.login);

router.post('/logout',loginSignup.logout);

router.get('/',(req,res)=>{ 
    return res.status(200).json({
        status: false,
        message: "this is home",
        status:200
       
      });
    
   
})

module.exports=router;
